#!/bin/bash

filename_backupFileDir="nextcloudFileDir.tar.gz"
filename_backupDataDir="nextcloudDataDir.tar.gz"
filename_backupDB="nextcloudDB.sql"

### Read out the date
currentDate=$(date +%Y%m%d%H%M)

#
# Please adjust everything from here to your configuration
#

#deletes backups that are older than 14 days
#backupalter="14"

# Directory where your data should be saved
backupMainDir="/backup"
backupdir="$backupMainDir/$currentDate"

# Directory in which your Nextcloud installation is located
nextcloudFileDir="/root/nextcloud/"

# Directory in which your Nextcloud user data is located, use docker inspect -f '{{ .Mounts }}' 
nextcloudDataDir="/var/lib/docker/volumes/nextcloud_data/"

# Name of your Nextcloud database
nextcloudDatabase="nextcloud"

# Name of your Nextcloud database user
dbUser="nextcloud"

# Password of your Nextcloud database user
dbPassword="KilmpodG65"
webserverUser="www-data"

# Name of your Nextcloud DB container
dockerdb="nextcloud_mysql_1"

#Name of your Nextcloud app container
dockerapp="nextcloud_php_1"

##### End of adjustments ######
#
# Output of the date
#
echo
echo backup of $ currentDate
echo

#
# Check for root
#
if [ "$(id -u)" != 0 ]   
then
        echo "ERROR: This script has to be run as root!"
        exit 1
fi

#
# Check if backup dir already exists
#
if [ ! -d "$backupdir" ]  
then
        mkdir -p "$backupdir"
else
        echo "ERROR: The backup directory $backupdir already exists!"
        exit 1
fi

#
# Set maintenance mode
#
echo "Maintenance mode for Nextcloud will be activated"
docker exec "$dockerapp" su -s /bin/sh "$webserverUser" -c "php occ maintenance:mode --on"
echo "Status: OK"
echo

#
# Backup file and data directory
#
echo "Creation of the backup of your Nextcloud installation"
tar -cpzf "$backupdir/$filename_backupFileDir" -C "$nextcloudFileDir" .
echo "Status: OK"
echo
echo "Creation of the backup of your Nextcloud data"
tar -cpzf "$backupdir/$filename_backupDataDir" -C "$nextcloudDataDir" .
echo "Status: OK"
echo

#
# Backup DB
#
echo "Creation of the backup of your Nextcloud database"
docker exec "$dockerdb" /usr/bin/mysqldump -u "$dbUser" -p"$dbPassword" "$nextcloudDatabase" > "$backupdir/$filename_backupDB"
echo "Status: OK"
echo

#
# Delete old files
#
#echo "files over" "$backupalter" "delete days"  
#find "$backupMainDir" -type f -mtime + "$backupalter" -delete
#echo "Delete empty folder"
#find "$backupMainDir" -type d -empty -delete

#
# Disable maintenance mode
#
echo "Deactivation of maintenance mode"
docker exec "$dockerapp" su -s /bin/sh "$webserverUser" -c "php occ maintenance:mode --off"
echo "Status: OK"
echo